<?php

ini_set('max_execution_time', 40000);
ini_set('memory_limit', '4G');

define('VERSAO', '1.0');
define('DESCRICAO', 'Ferramenta que realiza o restore de um arquivo que está na pasta z_data ou enviado via form.');

class DBRestore
{

    private $local = array();
    private $remoto = array();

    public function __construct()
    {
        $this->requires();
        $VERSAO = VERSAO;
        $DESCRICAO = DESCRICAO;
        $LIBS_EXTRA_JS = '';
        $TOOL = 'DBRestore';

        if (isset($_POST['enviar'])) {
            $this->lerVariaveis();
            $RESULTADO = $this->restaurar();
        } else {
            $this->defaultValues();
            $RESULTADO = '';
        }
        $LOCAL = $this->local;

        include __DIR__ . '/extras/cabecalho.phtml';
        include __DIR__ . '/extras/CloneBD/formRestore.phtml';
    }

    private function defaultValues()
    {
        $this->local['bd'] = DB_NAME;
        $this->local['server'] = DB_SERVER;
        $this->local['usuario'] =  DB_USER;
        $this->local['senha'] = DB_PASSWORD;
        $this->local['porta'] = 5432;
        $this->local['manterBancoAntigo'] = true;
        $this->local['arquivoLocal'] = true;
        $this->local['arquivoLocalInput'] = '';
    }

    private function lerVariaveis()
    {
        $this->local['bd'] = filter_input(INPUT_POST, 'bdLocal', FILTER_SANITIZE_STRING);
        $this->local['usuario'] = filter_input(INPUT_POST, 'usuarioLocal', FILTER_SANITIZE_STRING);
        $this->local['senha'] = filter_input(INPUT_POST, 'senhaLocal', FILTER_SANITIZE_STRING);
        $this->local['server'] = filter_input(INPUT_POST, 'serverLocal', FILTER_SANITIZE_STRING);
        $this->local['porta'] = filter_input(INPUT_POST, 'porta', FILTER_SANITIZE_NUMBER_INT);
        $this->local['arquivoLocal'] = filter_input(INPUT_POST, 'arquivoLocal', FILTER_VALIDATE_BOOLEAN);
        $this->local['manterBancoAntigo'] = filter_input(INPUT_POST, 'manterBancoAntigo', FILTER_VALIDATE_BOOLEAN);
        $this->local['arquivoLocalInput'] = filter_input(INPUT_POST, 'arquivoLocalInput', FILTER_DEFAULT);
    }

    private function getFile(){
        if($this->local['arquivoLocal']){
            return '/app/z_data/' . $this->local['arquivoLocalInput'];
        }
        return false;
    }

    private function restaurar()
    {
        ob_start();
        echo 'Iniciando o processo...' . PHP_EOL;
    
        $file = $this->getFile();

        if($file !== false){
            echo 'Arquivo determinado iniciando processo...' . PHP_EOL;

            $model = new CloneModel($this->local);
            $model->restore($file);
          
        }else{
            echo 'Não implementado ainda...' . PHP_EOL;
        }

        $this->backupPosConditions();


        $tempo = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        echo "Processado em: " . $tempo . ' segundos';

        $retorno = ob_get_contents();
        ob_end_clean();
        return $retorno;
    }

    private function backupPosConditions()
    {
        echo 'Verificando passos adicionais' . PHP_EOL;
        $remoto = $this->local;
        $local = $this->local;
        $file = __DIR__ . '/../build/backup_pos_conditions.php';
        if (file_exists($file)) {
            echo 'Arquivo existe prosseguindo ...' . PHP_EOL;
            require $file;
        } else {
            echo 'Arquivo NÃO existe.' . PHP_EOL;
        }
        echo 'FINALIZADO.' . PHP_EOL;
    }

    private function addData($backup)
    {
        ds($this->local);
        $db = new ModeloBD($this->local['bd'], $this->local['server'], $this->local['usuario'], $this->local['senha']);

        $db->DB()->debugOn();
        echo 'Banco preparado para receber o Backup, iniciando...';
        $handle = file($backup);
        $exec = '';
        $count =  0;
        $showLines = true;

        foreach ($handle as $linha) {
            $count++;
            if ($count < 500) {
                $exec .= $linha;
            } else {
               
                //echo 'entrei no exec';
                $insert =  substr($linha, 0, 6);

                if ($insert == 'INSERT') {
                    // echo $exec;
                    $count = 0;
                    try{
                        $db->DB()->exec($exec);
                    }catch(Exception $e){
                        die("Erro ao tentar executar" .$exec);
                    }
                    
                    $exec = $linha;
                } else {
                    $exec .= $linha;
                }
                echo '500 linhas executadas....' . PHP_EOL;
                $showLines = false;
            }
            if($showLines){
                echo $linha . PHP_EOL;
            }
        }

        $db->DB()->exec($exec); //Executar o resto

    }

    private function atualizaBD($backup)
    {
        $db = new ModeloBD($this->local['bd'], $this->local['server'], $this->local['usuario'], $this->local['senha']);

        if ($this->local['manterBancoAntigo']) {
            $oldSchema = 'old_' . date('d_m_hi');
            $db->DB()->exec('ALTER SCHEMA public RENAME TO ' . $oldSchema);
            $drop = $db->DB()->exec('CREATE SCHEMA public;');
            echo 'Schema atual renomeado para ' . $oldSchema . PHP_EOL;
        }

        

        if ($drop !== false) {

            echo 'Banco preparado para receber o Backup, iniciando...' . PHP_EOL;
            $this->addData($backup);
        }
    }

   

 

    private function requires()
    {
        require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
        require_once __DIR__ . '/extras/CloneBD/CloneModel.class.php';

    }
}
