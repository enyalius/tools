<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerador
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
abstract class Gerador
{

    /**
     *
     * @var ConfiguracaoGerador 
     */
    protected $conf;

    /**
     * Array associativo $chave => $valor para ser usado para substituições gerais tipo
     * agua => água
     *
     * @var Array
     */
    public static $dicionario = [];
    
    private $iniFileData = null;

    public function __construct()
    {
        $this->requires();
        $this->conf = new ConfiguracaoGerador();
        $this->carregaIni();

    }
    
       public function carregaIni()
    {
        $array = $this->iniFile();
        Gerador::$dicionario = $array['dicionario'];
    }

    public function iniFile()
    {
        if($this->iniFileData == null){
            $this->iniFileData = parse_ini_file(__DIR__ . '/../extras/dados.ini', true);
        }
        return $this->iniFileData;
    }

    public function requires()
    {
        require_once __DIR__ . '/ConfiguracaoGerador.class.php';
    }

}
