<?php

define('VERSAO', '1.0');

class InstallBD
{

    private $files = array();

    public function __construct()
    {
        $this->requires();
        $VERSAO = VERSAO;
        $LIBS_EXTRA_JS = '';
        $TOOL = "Instalador de banco de dados";
        
        $RESULTADO = $this->gerar();
        include __DIR__ . '/extras/cabecalho.phtml';
        include __DIR__ . '/extras/resultado.phtml';        
    }  

    private function gerar()
    {
        ob_start();
        echo 'Verificando a existência do arquivo install.php em build!' . PHP_EOL;
        $file =  ROOT .'/../build/install.php';
        if(file_exists($file)){
            echo 'Arquivo existe prosseguindo ...' . PHP_EOL;
            require $file;
        }else{
            echo 'Arquivo NÃO existe Utilize uma outra ferramenta acima ...' . PHP_EOL;
        }
        $retorno = ob_get_contents();
        ob_end_clean();
        return $retorno;
    }

   

    private function requires()
    {
       // require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
    }

}
