<?php

define('VERSAO', '1.7.0 03/10/2024');
require_once __DIR__ . '/base/Gerador.class.php';

/**
 * Classe responsável por gerar classes.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class GeradorDeCrud extends Gerador
{


    public static $ignores = [];

    public function __construct()
    {
        parent::__construct();
        $this->copyFiles();
        $this->requires();
        $this->trataForm();
        GeradorDeCrud::$ignores = $this->iniFile()['ignores']['noTpl'];
    }

    private function copyFiles(){
        if (isset($_POST['files'])) {
            require __DIR__ . '/extras/copiador.php';
            exit();
        }
    }

    private function trataForm()
    {
        $VERSAO = VERSAO;
        $TOOL = "Gerador de CRUD";
        $LIBS_EXTRA_JS = '';

        if (isset($_POST['enviar'])) {
            $RESULTADO = $this->gerar();
            $nomeDao = filter_input(INPUT_POST, 'nomeDao', FILTER_SANITIZE_STRING);
            $campos = filter_input(INPUT_POST, 'campos', FILTER_SANITIZE_STRING);
        } else {
            $RESULTADO = $nomeDao = $campos = '';
        }
        $CONF = $this->conf;
        $SCRIPT = $this->getScripts();
        $LIBS_EXTRA_JS = '';
        
        include __DIR__ . '/extras/cabecalho.phtml';
        include __DIR__ . '/geradorCrud/view/form.phtml';
        include __DIR__ . '/extras/resultado.phtml';

    }

    private function getScripts()
    {
        return file_get_contents(__DIR__ . '/geradorCrud/interface.js');
    }

    public function preparaPastaClasses()
    {
        echo '-- Criando diretórios';
        system('rm -rf /tmp/eny/classes/*');
        mkdir('/tmp/eny/classes/control', 0775, true);
        mkdir('/tmp/eny/classes/model', 0775, true);
        mkdir('/tmp/eny/classes/model/DTO', 0775, true);
        mkdir('/tmp/eny/classes/view', 0775, true);
        mkdir('/tmp/eny/classes/view/templates', 0775, true);
        mkdir('/tmp/eny/classes/view/templates/forms', 0775, true);
        mkdir('/tmp/eny/classes/model/DAO', 0775, true);
        if ($this->conf->isAdmin()) {
            mkdir('/tmp/eny/classes/control/admin', 0775, true);
        }
    }

    public function requires()
    {
        parent::requires();
        require_once __DIR__ . '/geradorCrud/CampoGerador.class.php';
        require_once __DIR__ . '/geradorCrud/TabelaGerador.class.php';
        require_once __DIR__ . '/geradorCrud/ArquivoGerador.class.php';
        require_once __DIR__ . '/geradorCrud/ModeloGerador.class.php';
        require_once __DIR__ . '/geradorCrud/ControleGerador.class.php';
        require_once __DIR__ . '/geradorCrud/DataTransferGerador.class.php';
        require_once __DIR__ . '/geradorCrud/ColunaGerador.class.php';
        require_once __DIR__ . '/geradorCrud/TemplateGerador.class.php';
        require_once __DIR__ . '/geradorCrud/TituloTabelaGerador.class.php';
        require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
    }

    private function gerarApenasDataAccess($nomeDao, $campos)
    {
        echo '<pre>';
        $dao = $this->geraDataTransfer($nomeDao, $campos);
        echo htmlspecialchars($dao);
    }

    private function gerar()
    {
        ob_start();
        $this->setarConfiguracoes($_POST);
        $this->executaGerador();

        $retorno = ob_get_contents();
        ob_end_clean();
        return $retorno;
    }

    private function executaGerador()
    {
        extract($_POST);
        $nomeDao = filter_input(INPUT_POST, 'nomeDao', FILTER_SANITIZE_STRING);
        $campos = filter_input(INPUT_POST, 'campos', FILTER_SANITIZE_STRING);

        if (isset($gerarBanco)) {
            $dtos = empty($nomeDao) ? false : $nomeDao;
            $this->preparaPastaClasses();
            $this->gerarBanco($nomeBanco, $dtos);
        } else {
            $this->gerarApenasDataAccess($nomeDao, $campos);
        }
    }

    private function setarConfiguracoes($conf)
    {

        $this->conf->setAutor($conf['autor'])
                ->setEmailAutor($conf['emailAutor'])
                ->setBanco($conf['nomeBanco']);

        $this->conf->setGerarBanco(isset($conf['gerarBanco']) ? $conf['gerarBanco'] : false);
        $this->conf->setDeletar(isset($conf['deletar']) ? $conf['deletar'] : false);
        $this->conf->setRewrite(@$_POST['rewrite']);
        $this->conf->setUrlAdicional(@$_POST['urlAdicional']);
        $this->conf->setAdmin(isset($conf['modoAdmin']) ? $conf['modoAdmin'] : false);

        //Em standby a partir da versão 1 do enyalius
        $int = isset($conf['internacionalizacao']) ? $conf['internacionalizacao'] : false;
        $this->conf->setInternacionalizacao($int);

        $esquemasGlobais = array(
            'public' => '',
            'system' => 'admin'
        );

        $GLOBALS['bancoPadrao'] = true;
        $GLOBALS['urlAdicional'] = $conf['urlAdicional'];

        $this->conf->setEsquemas($esquemasGlobais);
    }

    function geraControlador(\TabelaGerador $tabela = null)
    {
        $controlador = new ControleGerador();

        $controlador->setTabela($tabela);
        $controlador->setNome(ucfirst($tabela->getNomeTabela()));
        $controlador->setSchema($tabela->getSchema());
        $controlador->setConfig($this->conf);

        return $controlador->gerar();
    }

    private function geraDataTransfer($nomeDTO, $campos, \TabelaGerador $tabelaObj = null)
    {
        $dataTransfer = new DataTransferGerador();
        $dataTransfer->setConfig($this->conf);
        if ($tabelaObj == null) {
            $tabelaObj = new TabelaGerador($nomeDTO);
            $tabelaObj->setColunas(explode(',', $campos));
        }
        $dataTransfer->setTabela($tabelaObj);
        return $dataTransfer->gerar();
    }

    private function geraDAO(\TabelaGerador $tabela = null)
    {
        $modelo = new ModeloGerador();
        $modelo->setTabela($tabela);
        $modelo->setConfig($this->conf);
        return $modelo->gerar();
    }

    function geraTpl(\TabelaGerador $tabela = null)
    {
        $tpl = new TemplateGerador();
        $tpl->setTabela($tabela);
        $tpl->setConfig($this->conf);
        return $tpl->gerar();
    }

    private function montaQueryTabelas($dtos)
    {
        $query = "SELECT   schemaname AS esquema,
                        tablename AS tabela
               FROM pg_catalog.pg_tables
                                                WHERE schemaname NOT IN
                                                ('pg_catalog',
                                                'information_schema',
                                                'pg_toast')";
        if ($dtos) {
            list($tabelas, $schemas, $tabelaEspecificas) = $this->trataTabelas($dtos);
            $query .= ' AND ';
            if (sizeof($tabelaEspecificas) > 0) {
                echo '* tabela em eschemas especificos =>' . PHP_EOL;
                foreach ($tabelaEspecificas as $parte) {
                    $query .= " ( tablename IN ('" . $parte[1] . "') AND schemaname IN ('" . $parte[0] . "') ) OR";
                    echo "        ( tablename IN ('" . $parte[1] . "') AND schemaname IN ('" . $parte[0] . "') ) OR" . PHP_EOL;
                }
            }
            if (!empty($tabelas)) {
                $query .= " tablename IN ('" . $tabelas . "') OR";
                echo '* Tabelas Selecionadas =>' . $tabelas . PHP_EOL;
            }
            if (!empty($schemas)) {
                $query .= " schemaname IN ('" . $schemas . "') OR";
                echo '* Schemas completos selecionadas =>' . $schemas . PHP_EOL;
            }
            $query = rtrim($query, ' OR');
        }
        $query .= "                               ORDER BY schemaname, tablename";
        return $query;
    }

    public function gerarBanco($nomeBanco, $dtos = false)
    {
        echo '<h1>Gerando arquivos do banco ' . $nomeBanco . '</h1>' . PHP_EOL;
        $modelo = new ModeloBD($nomeBanco);
        $consulta = $modelo->query($this->montaQueryTabelas($dtos));

        $esquemaAtual = '';
        while ($linha = $modelo->resultadoAssoc($consulta)) {
            if ($linha['tabela'] == 'custom_fields_lists') {
                continue;
            }
            $esquema = $linha['esquema'];
            $dir = '/tmp/eny/classes/';
            $admin = $this->conf->isAdmin() ? 'admin/' : '';

            if ($esquema != $esquemaAtual) {
                $esquemaAtual = $esquema;
                $dirModulo = isset($this->conf->getEsquemas()[$esquema]) ? ($this->conf->getEsquemas()[$esquema]) : $esquema;
                $dirModulo = $dirModulo == 'public' ? '' : $dirModulo;
                if (!is_dir('/tmp/eny/classes/' . $dirModulo)) {
                    echo 'Criando diretório ' . $dirModulo . PHP_EOL;
                    mkdir('/tmp/eny/classes/control/' . $admin . $dirModulo, 0777, true);
                    mkdir('/tmp/eny/classes/model/DAO/' . $dirModulo, 0777, true);
                    mkdir('/tmp/eny/classes/model/DTO/' . $dirModulo, 0777, true);
                    mkdir('/tmp/eny/classes/view/templates/' . $dirModulo, 0777, true);
                    mkdir('/tmp/eny/classes/view/templates/' . $dirModulo . '/forms', 0777, true);
                }
            }
            $tabela = new TabelaGerador($esquema . '.' . $linha['tabela']);

            

            $tabela->carrega($modelo);

            echo '<h3>' . $tabela->getLabel() . '</h3>';
           
            $dirModulo = $dirModulo ? $dirModulo . '/' : ''; 

            $identificador =  $dirModulo . $tabela->getNomeCamelCase();
            $id = str_replace('/', '_', $identificador);


            $fileDTO = $dir . 'model/DTO/' . $identificador . '.class.php';
            echo '<a class="btnSelecionarTudo btn btn-default" href="#" data-ids="' . $id . '"> Selecionar tudo </a> <hr />';


            echo '<br> <input class="file ' . $id . '" value="'. $fileDTO . '" type="checkbox" /> ';
            echo '<a href="PreviewFile?file='. str_replace($dir, '', $fileDTO).'" target="_blank">Gerando Classe DTO ' . $tabela->getNomeCamelCase() . '</a>';
            $codigo = $this->geraDataTransfer($tabela->getLabel(), false, $tabela);
            file_put_contents($fileDTO, $codigo);
            echo "<br><hr><br>";

            
            $fileControlador = $dir . 'control/' . $admin . $dirModulo . 'Controlador' . $tabela->getNomeCamelCase() . '.class.php';
            echo '<br> <input class="file ' . $id . '" value="'. $fileControlador. '" type="checkbox" /> ';
            echo '<a href="PreviewFile?file='. str_replace($dir, '', $fileControlador).'" target="_blank">Gerando Classe Controlador' . $tabela->getLabel() . '.class.php </a>' . PHP_EOL;
            $codigoControlador = $this->geraControlador($tabela);
            file_put_contents($fileControlador, $codigoControlador);
            echo "<br><hr><br>";

            $fileDAO = $dir . '/model/DAO/' . $dirModulo . '/' . $tabela->getNomeCamelCase() . 'DAO.class.php';
            echo '<br> <input class="file ' . $id . '" value="'. $fileDAO . '" type="checkbox" /> ';
            echo '<a href="PreviewFile?file='. str_replace($dir, '', $fileDAO).'" target="_blank">Gerando Classe DAO ' . $tabela->getLabel() . 'DAO.class.php </a>' . PHP_EOL;
            $texto = $this->geraDAO($tabela);
            file_put_contents($fileDAO, $texto);
            echo '<br><hr><br>';

            $fileTemplate = ($dir . 'view/templates/' . $dirModulo . '/forms/' . $tabela->getNomeTabela() . '.tpl');

            echo '<br> <input class="file ' . $id . '" value="'. $fileTemplate. '" type="checkbox" /> ';
            echo ' <a href="PreviewFile?file='. str_replace($dir, '', $fileTemplate).'" target="_blank">Gerando Template ' . $tabela->getLabel() . '.tpl</a>' .  PHP_EOL;
            $codigo = $this->geraTpl($tabela, $tabela);
            file_put_contents($fileTemplate, $codigo);
            echo '<br><hr><br>';
        }
        echo '<a id="btnCopiar" class="btn btn-danger"> Copiar arquivos </a>';
    }

    private function trataTabelas($string)
    {
        $tabelas = $schemas = '';
        $listaTabelas = $listaSchemas = $tabelasComEsquema = array();

        $itens = explode(',', str_replace("\n", '', $string));

        if (count($itens) > 1) {
            foreach ($itens as $tabela) {
                $result = $this->extractSchema($tabela);
                if (is_array($result) && sizeof($result) > 1) {
                    $tabelasComEsquema[] = $result;
                } else if (is_array($result)) {
                    $listaSchemas = $result[0];
                } else {
                    $listaTabelas[] = $result;
                }
            }
            $tabelas = implode("', '", $listaTabelas);
            $tabelas = rtrim($tabelas, ", '");

            $schemas = implode("', '", $listaSchemas);
            $schemas = rtrim($schemas, ", '");
        } else {
            $result = $this->extractSchema($string);
            if (is_array($result) && sizeof($result) > 1) {
                $tabelasComEsquema[] = $result;
            } else if (is_array($result)) {
                $schemas = $result[0];
            } else {
                $tabelas = $result;
            }
        }
        return array($tabelas, $schemas, $tabelasComEsquema);
    }

    private function extractSchema($tabela)
    {
        $tabela = trim((string) $tabela);
        $pieces = explode('.', $tabela);
        if (count($pieces) > 1) {
            if ($pieces[1] == '*') {
                return array($pieces[0]);
            } else {
                return $pieces;
            }
        } else {
            return $tabela;
        }
    }

 

}
