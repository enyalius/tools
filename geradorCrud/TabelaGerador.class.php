<?php

/**
 * Classe responsável por ler os metadados de Tabela
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 2015-12-12
 */
class TabelaGerador
{

    private $nomeCompleto;
    private $nomeTabela;
    private $tituloTabela;
    private $schema;
    private $colunas = [];
    private $chavePrimaria = [];
    private $camposGeograficos = [];
    private $camposArquivos = [];
    private $camposImagens = [];

    public function __construct($nomeCompleto)
    {
        if (strpos($nomeCompleto, '.') !== false) {
            list($this->schema, $this->nomeTabela) = explode('.', $nomeCompleto);
            $this->nomeCompleto = $nomeCompleto;
        } else {
            $this->schema = 'public';
            $this->nomeTabela = $nomeCompleto;
            $this->nomeCompleto = "{$this->schema}.{$this->nomeTabela}";
        }
        $this->tituloTabela = new TituloTabelaGerador($this->nomeTabela);
    }

    /**
     * Retorna a quantidade de campos arquivos que o
     * @return int
     */
    public function possuiArquivo()
    {
        return count($this->camposArquivos);
    }

    public function possuiCamposGeograficos()
    {
        return count($this->camposGeograficos);
    }

    /**
     * Retorna a quantidade de campos arquivos para imagens
     * 
     * @return int
     */
    public function possuiImagens()
    {
        return count($this->camposImagens);
    }

    public function getCamposArquivos()
    {
        return $this->camposArquivos;
    }

    public function getCamposImagens()
    {
        return $this->camposImagens;
    }

    public function setColunas($colunas)
    {
        foreach ($colunas as $coluna) {
            $this->colunas[] = new ColunaGerador($coluna);
        }
    }

    /**
     *
     * @return string Nome da tabela completo com Schema e nome da tabela
     */
    public function getNomeCompleto()
    {
        return $this->nomeCompleto;
    }

    public function getSchema()
    {
        return $this->schema;
    }

    /**
     *
     * @return string Nome da tabela sem schema
     */
    public function getNomeTabela()
    {
        return $this->nomeTabela;
    }

    /**
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->tituloTabela->getLabel();
    }

    /**
     *
     * @return array<ColunaGerador> - Lista de objetos do tipo
     */
    public function getColunas()
    {
        return $this->colunas;
    }

    public function getChavePrimaria()
    {
        return $this->chavePrimaria;
    }

    public function getChavePrimariaAsString()
    {
        return rtrim(implode(',', $this->chavePrimaria), ',');
    }

    public function getNomeCamelCase()
    {
        return ucfirst($this->tituloTabela->getVariavel());
    }

    /**
     * Verifica se a tabela possui chave composta 
     * 
     * @return boolean
     */
    public function isChaveComposta()
    {
        if (sizeof($this->chavePrimaria) > 1) {
            return true;
        } else {
            return false;
        }
    }

    public function carrega(ModeloBD $modelo)
    {
        $consulta = $modelo->query($this->queryTable($this->schema, $this->nomeTabela));

        $colunas = array();
        $i = 0;
        foreach ($consulta as $linha) {
            $colunas[$linha['name']] = array(
                'col_name' => $linha['name'],
                'is_null' => $linha['notnull'],
                'data_type' => $linha['type']);


            $coluna = new ColunaGerador($linha['name']);
            $coluna->setTipo($linha['type']);

            $coluna->setNotNull($linha['notnull']);
            $coluna->setChavePrimaria($linha['primarykey']);
            $coluna->setChaveEstrangeira($linha['foreignkey']);
            if (isset($this->colunas[$linha['number']])) {
                $this->colunas[$linha['number']] = $this->merge($coluna, $this->colunas[$linha['number']]);
            } else {
                $this->colunas[$linha['number']] = $coluna;
            }

            if ($coluna->isChavePrimaria()) {
                $this->addChavePrimaria($coluna);
            }
            $coluna->verificaObjeto();

            if ($coluna->isGeo()) {
                $this->camposGeograficos[] = $coluna;
            }

            if ($coluna->isArquivo()) {
                $this->camposArquivos[] = $coluna;
            }

            if ($coluna->isImagem()) {
                $this->camposImagens[] = $coluna;
            }
        }
    }

    /**
     * 
     * @param ColunaGerador $x
     * @param ColunaGerador $y
     * @return \ColunaGerador
     */
    private function merge(ColunaGerador $x, ColunaGerador $y)
    {
        if ($x->isNotNull() || $y->isNotNull()) {
            $x->setNotNull(true);
        }
        if ($x->isChaveEstrangeira() || $y->isChaveEstrangeira()) {
            if (!empty($x->getChaveEstrangeiraRelacao())) {
                $chave = $x->getChaveEstrangeiraRelacao();
            } else {
                $chave = $y->getChaveEstrangeiraRelacao();
            }
            $x->setChaveEstrangeira($chave);
        }
        if ($x->isChavePrimaria() || $y->isChavePrimaria()) {
            $x->setChavePrimaria(true);
        }
        return $x;
    }

    /**
     * Método que adicionas as colunas que são chave primária.
     * 
     * @param ColunaGerador $chave
     */
    private function addChavePrimaria(ColunaGerador $chave)
    {
        $this->chavePrimaria[$chave->getNome()] = $chave;
    }

    /**
     * 
     * @param string $schema
     * @param string $table
     * @return string
     */
    private function queryTable($schema, $table)
    {
        return "SELECT
                    f.attnum AS number, f.attname AS name, f.attnum, f.attnotnull AS notnull,
                    pg_catalog.format_type(f.atttypid,f.atttypmod) AS type,
                    CASE WHEN p.contype = 'p' THEN 't' ELSE 'f'
                    END AS primarykey,
                    CASE WHEN p.contype = 'u' THEN 't' ELSE 'f'
                    END AS uniquekey,
                    CASE WHEN p.contype = 'f' THEN g.relname
                    END AS foreignkey
                    --,
                    --CASE WHEN f.atthasdef = 't' THEN d.adbin
                    --END AS default
                FROM pg_attribute f
                    JOIN pg_class c ON c.oid = f.attrelid 
                    --LEFT JOIN pg_attrdef d ON d.adrelid = c.oid AND d.adnum = f.attnum
                    LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
                    LEFT JOIN pg_constraint p ON p.conrelid = c.oid AND f.attnum = ANY (p.conkey)
                    LEFT JOIN pg_class AS g ON p.confrelid = g.oid
                WHERE c.relkind = 'r'::char
                    AND n.nspname = '" . $schema . "' AND c.relname = '" . $table . "'
                    AND f.attnum > 0 ORDER BY number";
    }

}
