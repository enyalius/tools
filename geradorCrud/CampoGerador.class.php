<?php

/**
 * Classe que controla os campos, os tipos e os dados permitindo a fácil
 * manipulação deles
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class CampoGerador
{


    /**
     * Campos que devem ser ignorados na inserção
     *
     * @var array
     */
    protected $camposIgnoreInsert = [
        'data_cadastro', 'data_ultima_alteracao'
    ];

    /**
     * Campos que devem ser ignorados na consulta
     *
     * @var array
     */
    protected $camposIgnoreJson = [];

    /**
     * Campos que devem ser ignorados nos templates
     *
     * @var array
     */
    protected $camposIgnoreTemplates = [
        'data_cadastro', 'data_ultima_alteracao'
    ];

    /**
     * Variável que determina o nome do campo no formato de Banco de dados.
     * EX: data_coleta
     *
     * @var string
     */
    protected $nome;

    /**
     * Variável que apresenta o campo em formato camelCase
     * Ex: dataColeta
     *
     * @var string
     */
    protected $variavel;

    /**
     * Variável que apresenta o campo como apelido para formulários e etc
     * Ex: Data coleta
     *
     * @var string
     */
    protected $label;

    /**
     * Verifica se o campo tem underscore e number para resolver problema de campos
     * 
     * Tipo 0: Não possui
     * Tipo 1: questao_1_texto - Apenas marca
     * Tipo 2: questao_1_11 x questao_11_1 - Troca o underscore entre os numbers por 'p' => questao_1p11 x questao_11p1
     *
     * @var int
     */
    protected $underscoreNumber = 0;


    public function ignorarEmTpl()
    {
        if (in_array($this->nome, GeradorDeCrud::$ignores)) {
            return true;
        } else {
            return false;
        }
    }

    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Retorna o nome da coluna no padrao nomeColuna
     *
     * @return string
     */
    public function getVariavel()
    {
        return $this->variavel;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setVariavel($variavel)
    {
        $this->variavel = $variavel;
        return $this;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    protected function singulariza($palavra)
    {
        StringUtil::singular($palavra);
    }

    /**
     * Gera nome do campo como Variavel e atribui para $this->variavel.
     *
     */
    protected function geraVariavelCamelCase()
    {
        $nome = $this->nome;

        $padrao = '/(_\d)_(\d+)/';; // Encontra o underscore seguido de números e undescore tipo 2
        preg_match_all($padrao, $nome, $correspondencias);
        if(count($correspondencias[0]) > 0) {
            $this->underscoreNumber = 2;
            $nome = preg_replace($padrao, '$1p$2', $nome);
        } else{
            $padrao = '/(_\d+)/'; // Encontra o underscore seguido de números
            preg_match_all($padrao, $nome, $correspondencias);
            if(count($correspondencias[0]) > 0) {
                $this->underscoreNumber = 1;
            } 
        }

    

        $this->variavel = StringUtil::toCamelCase($nome);
    }

    /**
     * Gera nome do campo como apelido e atribui para $this->label.
     *
     */
    protected function geraLabel()
    {
        $aux = str_replace('_', ' ', $this->nome);
        $aux = str_ireplace('id ', '', $aux);
        foreach (Gerador::$dicionario as $str => $strSub) {
            $aux = str_replace($str, $strSub, $aux);
        }
        $this->label = ucfirst($aux);
    }


    /**
     * Tipo 0: Não possui
     * Tipo 1: questao_1_texto - Apenas marca
     * Tipo 2: questao_1_11 x questao_11_1 - Troca o underscore entre os numbers por 'p' => questao_1p11 x questao_11p1
     *
     * @return  int
     */ 
    public function getUnderscoreNumber()
    {
        return $this->underscoreNumber;
    }
}
