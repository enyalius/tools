<?php

/**
 * Classe responsável por gerar o modelo de dados.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package tools.geradorCrud
 * @version 1.7.0
 */
class ModeloGerador extends ArquivoGerador
{

    public function gerar()
    {

        $print = '<?php' . PHP_EOL;
        $print .= PHP_EOL;
        $print .= '/**' . PHP_EOL;
        $print .= ' * Classe de modelo referente ao objeto ' . ucfirst($this->tabela->getNomeCamelCase()) . ' para ' . PHP_EOL;
        $print .= ' * a manutenção dos dados no sistema ' . PHP_EOL;
        $print .= ' *' . PHP_EOL;
        $print .= ' * @package app.' . lcfirst($this->getModulo()) . PHP_EOL;
        $print .= ' * @author ' . $this->config->getAutor() . ' <' . $this->config->getEmailAutor() . '>' . PHP_EOL;
        $print .= ' * @version 1.0.0 - ' . date('d-m-Y') . '(Gerado automaticamente - GC - ' . VERSAO . ')' . PHP_EOL;
        $print .= ' */' . PHP_EOL . PHP_EOL;
        $print .= 'class ' . $this->tabela->getNomeCamelCase() . 'DAO extends AbstractDAO ' . PHP_EOL . '{';
        $print .= PHP_EOL . PHP_EOL;
        $print .= '    /**' . PHP_EOL;
        $print .= '    * Construtor da classe ' . $this->nome . 'DAO esse metodo  ' . PHP_EOL;
        $print .= '    * instancia o Modelo padrão conectando o mesmo ao banco de dados' . PHP_EOL;
        $print .= '    *' . PHP_EOL;
        $print .= '    */' . PHP_EOL;
        $print .= '    public function __construct()' . PHP_EOL . '    {' . PHP_EOL;
        if ($GLOBALS['bancoPadrao']) {
            $print .= "        parent::__construct();" . PHP_EOL;
        } else {
            $print .= "        parent::__construct('" . $this->config->getBanco() . "', '" . $this->config->getUsuarioBanco() . "', '" . $this->config->getSenhaBanco() . "');" . PHP_EOL;
        }
        $print .= PHP_EOL;
        $print .= "        \$this->table =  {$this->nome}::table();" . PHP_EOL;
        $print .= "        \$this->colunmID = '" . $this->tabela->getChavePrimariaAsString() . "';" . PHP_EOL;
        $print .= "        \$this->colunms = [ " . $this->getColunas() . PHP_EOL;
        $print .= '                          ];' . PHP_EOL;
        $print .= '    }' . PHP_EOL;
        $print .= PHP_EOL;

        $print .= $this->setDados();
        return $print .= '}';
    }

    private function getColunas()
    {
        $campos = $this->tabela->getColunas();
        
        array_shift($campos); //remove o id

        $campo = array_shift($campos);        
        $print = "    '" . $this->getColuna($campo) . "'," . PHP_EOL;
        foreach ($campos as $campo) {
            $print .= "                                " . $this->getCampoIndexado($campo) ;
            $print .= "'" . $this->getColuna($campo) . "'," . PHP_EOL;
        }
        
        return rtrim($print, "," . PHP_EOL);
    }

    /**
     * Indexa campos que tem underscore com number para evitar erros
     *
     * @param ColunaGerador $coluna
     * @return string|void
     */
    private function getCampoIndexado(ColunaGerador $coluna)
    {
        if($coluna->getUnderscoreNumber() > 0){
            return "'" . $coluna->getVariavel(). "' => ";
        }

    }

    private function getColuna(ColunaGerador $coluna)
    {
        if ($coluna->isGeo()) {
            return 'ST_ASTEXT(' . $coluna->getNome() . ') as ' . $coluna->getNome();
        }
        return $coluna->getNome();
    }

    private function setDados()
    {
        $print = '    /**' . PHP_EOL;
        $print .= '     * Retorna um objeto setado ' . $this->nome . PHP_EOL;
        $print .= '     * com objetivo de servir as funções getTabela, getLista e get' . ucfirst($this->nome) . PHP_EOL;
        $print .= '     *' . PHP_EOL;
        $print .= '     * @param array $dados' . PHP_EOL;
        $print .= '     * @return ' . $this->nome . PHP_EOL;
        $print .= '     */' . PHP_EOL;
        $print .= '    protected function setDados($dados)' . PHP_EOL . '    {' . PHP_EOL;
        $print .= '        $' . lcfirst($this->nome) . ' = new ' . $this->nome . '();' . PHP_EOL;
        $print .= '        $this->dataBuilder($' . lcfirst($this->nome) . ', $dados);' . PHP_EOL;
      
        $print .= '        return $' . lcfirst($this->nome) . ';' . PHP_EOL;
        $print .= '    }' . PHP_EOL;

        return $print;
    }

    private function getObjeto(ColunaGerador $campo)
    {
        if ($campo->isGeo()) {
            return 'new ' . $campo->geometria() . "(\$dados['" . $campo . "'])";
        }
        return "\$dados['" . $campo . "']";
    }

    private function arquivoEmBanco()
    {
        
    }

    private function arquivoEmDir()
    {
        $print = PHP_EOL;
        $print .= 'private function inserirArquivo(' . $this->nome . ' $obj, Foto $foto, $alterar = false) {' . PHP_EOL;
        $print .= '$nomeArq = $produto->geraNome();' . PHP_EOL;
        
        $print .= "  \$endFisico = ROOT . 'media/';" . PHP_EOL;
        /* $endLogico = '/media/';

        if (!$alterar) {
            $fotoProduto = new ArquivoUpload($_FILES['caminhoFoto']);
            $r = new Redimensionador($fotoProduto->getArquivo(), $endFisico . $nomeArq . '.jpg', 272, 272);
            $r2 = new Redimensionador($fotoProduto->getArquivo(), $endFisico . $nomeArq . '_mini.jpg', 100, 100);
            $foto->setCaminhoFoto($endLogico . $nomeArq . '.jpg');
            $foto->setIdProduto($produto->getIdProduto());
            return $r && $r2;
        } else {
            $end = ROOT . '../www' . $_POST['foto'];
            $miniEnd = str_replace('.jpg', '_mini.jpg', $end);

            $fotoProduto = new ArquivoUpload($_FILES['caminhoFotoSubstituir']);
            if ($fotoProduto->isOk()) {
                unlink($end);
                unlink($miniEnd);
                $r = new Redimensionador($fotoProduto->getArquivo(), $end, 272, 272);
                $r2 = new Redimensionador($fotoProduto->getArquivo(), $miniEnd, 100, 100);
                return $r && $r2;
            }
        }*/
        $print .= '         return true;' . PHP_EOL;
        $print .= '     }' . PHP_EOL;
        return $print;
    }

}
