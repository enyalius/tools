<?php

/**
 * Classe que gera o DataTransfer 
 * 
 * Versão 1.1 - Vários métodos genéricos movidos para o DTOTrait gerando um DTO mais genérico. 
 *              Necessário Enyalius 4.0 ou superior
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.1.0
 */
class DataTransferGerador extends ArquivoGerador
{

    private $comTabela = true;
    private $interfaces;

    public function __construct()
    {
        $this->interfaces = ['DTOInterface'];
    }

    public function gerar()
    {
        $_confs = $this->config;

        $print = '<?php' . PHP_EOL;
        $print .= '/**' . PHP_EOL;
        $print .= ' * Classe para a transferencia de dados de ' . $this->nome . ' entre as ' . PHP_EOL;
        $print .= ' * camadas do sistema ' . PHP_EOL;
        $print .= ' *' . PHP_EOL;
        $print .= ' * @package app.model.dto' . PHP_EOL;
        $print .= ' * @author  ' . $_confs->getAutor() . ' <' . $_confs->getEmailAutor() . '> ' . PHP_EOL;
        $print .= ' * @version 1.0.0 - ' . date('d-m-Y') . '(Gerado Automaticamente com GC - ' . VERSAO . ')' . PHP_EOL;
        $print .= ' */' . PHP_EOL . PHP_EOL;

        $print .= "class " . $this->nome . " implements " . $this->verificaInterfaces() . PHP_EOL . '{' . PHP_EOL;
        //Imprime as variaveis
        $print .= '    use core\\model\\DTOTrait;' . PHP_EOL . PHP_EOL;
        foreach ($this->tabela->getColunas() as $campo) {
            $print .= ( '    private $' . $campo->getVariavel() . ';' . PHP_EOL);
        }
        $print .= '    private $isValid;' . PHP_EOL;
        if ($this->comTabela) {
            $print .= '    private $table;' . PHP_EOL;
            //Imprime o construtor
            $print .= $this->geraConstrutor($this->tabela->getNomeCompleto());
        }

        //Imprime metodos get and Set
        foreach ($this->tabela->getColunas() as $i => $campo) {
            $print .= $this->geraGettersAndSetters($campo);
        }

        $print .= $this->getID();
        $print .= $this->setID();
        $print .= $this->getCondicao();

        $print .= '}' . PHP_EOL;
        
        return $print;
    }

   
    /**
     *
     * @param string $tabela
     * @return string
     */
    private function geraConstrutor($tabela)
    {
        $print = PHP_EOL;
        $print .= '    /**' . PHP_EOL;
        $print .= '     * Construtor da classe responsável por setar a tabela ' . PHP_EOL;
        $print .= '     * e inicializar outras variáveis' . PHP_EOL;
        $print .= '     *' . PHP_EOL;
        $print .= '     * @param string $table -  Nome da tabela no banco de dados' . PHP_EOL;
        $print .= '     */' . PHP_EOL;
        $print .= '    public function __construct($table = ' . "'$tabela'" . ')' . PHP_EOL;
        $print .= '    {' . PHP_EOL;
        $print .= '        $this->table = $table;' . PHP_EOL;
        if ($this->tabela->possuiCamposGeograficos()) {
            $print .= '        core\\libs\\geo\\Geo::load();' . PHP_EOL;
        }
        $print .= '    }' . PHP_EOL;
        return $print;
    }

    private function geraGettersAndSetters(ColunaGerador $campo)
    {
        if ($campo->getVariavel() == 'id') {
            return;
        }
        $print = '';
        $print .= $this->getCampoData($campo);
        $validacao = $campo->getValidacaoPHP();
        if ($validacao) {       
            $print .= PHP_EOL;
            $print .= '    /**' . PHP_EOL;
            $print .= '     * Método que seta o valor da variável ' . $campo->getVariavel() . PHP_EOL;
            $print .= '     *' . PHP_EOL;
            $print .= '     * @param ' . $campo->getTipoPHP() . ' $' . $campo->getVariavel() . ' - Valor da variável ' . $campo->getVariavel() . PHP_EOL;
            $print .= '     */' . PHP_EOL;
            $print .= '    public function set' . ucfirst($campo->getVariavel()) . '(' . $campo->getTipagem() . '$' . $campo->getVariavel() . ')' . PHP_EOL . '    {' . PHP_EOL;
            $print .=          $validacao;
            $print .= '        return $this;' . PHP_EOL;
            $print .= '    }' . PHP_EOL;
        }
        return $print;
    }

    private function getCampoData($campo)
    {
        $print = '';
        if (stripos($campo, 'data') !== false) {
            $print .= PHP_EOL;
            $print .= '    /**' . PHP_EOL;
            $print .= '     * Retorna o valor da variável ' . $campo->getVariavel() . ' formatada ' . PHP_EOL;
            $print .= '     *' . PHP_EOL;
            $print .= '     * @param bool $comHora - opção para mostrar a hora ou não ' . PHP_EOL;
            $print .= '     * @param bool $extenso - opção para retornar a data por extenso ou não ' . PHP_EOL;
            $print .= '     * @return string - Valor da variável ' . $campo->getVariavel() . ' formatada ' . PHP_EOL;
            $print .= '     */' . PHP_EOL;
            $print .= '    public function get' . ucfirst($campo->getVariavel()) . 'Formatada($comHora = true, $extenso = true)' . PHP_EOL;
            $print .= '    {' . PHP_EOL;
            $print .= '        return $extenso ? DateUtil::formataDataExtenso($this->' . $campo->getVariavel() . ', $comHora) : DateUtil::formataData($this->' . $campo->getVariavel() . ', $comHora);' . PHP_EOL;
            $print .= '    }' . PHP_EOL;
        }
        return $print;
    }

    private function verificaInterfaces()
    {
        foreach ($this->tabela->getColunas() as $coluna) {
            if ($coluna->getTipo() == 'oid' || $coluna->getTipo() == 'bytea') {
                // $this->interfaces['ObjetoInsercao'] = 'ObjetoInsercao';
            }
        }
        $interfaces = '';

        foreach ($this->interfaces as $in) {
            $interfaces .= $in . ', ';
        }

        return rtrim($interfaces, ', ');
    }

    /*
     * 
     */
    private function setID()
    {
        if (!$this->comTabela) {
            return false;
        }
        $print = PHP_EOL;
        $print .= '    /**' . PHP_EOL;
        $print .= '     * Utiliza como condição de seleção a chave primária' . PHP_EOL;
        $print .= '     *' . PHP_EOL;
        $print .= '     * @return string - Condição para selecionar um dado unico na tabela' . PHP_EOL;
        $print .= '     */' . PHP_EOL;
        
        $chaves = $this->tabela->getChavePrimaria();
        if (sizeof($chaves) === 0) {//Sem chave
            $print .= '    public function setID()' . PHP_EOL . '    {' . PHP_EOL;
            echo '<span style="warning">[Warning]</span> - Tabela ' . ' possivelmente modelada errada - (sem chave primária)';
            $print .= '        return false;' . PHP_EOL;
            $print .= '     }' . PHP_EOL;
            return $print;
        } else {
            if(sizeof($chaves) > 1){// chave composta
                $print .= '    public function setIDs(';

                foreach($chaves as $chave){
                    $print .= '$' . $chave->getVariavel() . ', ';
                }
                $print = rtrim($print, ', ') . ')' . PHP_EOL . '    {' . PHP_EOL;
                foreach($chaves as $chave){
                    $print .= $chave->getValidacaoPHP();
                }
            }else{//Usar o método padrão de setID.
                return '';
            }         
        }        
        $print .= '        return $this;' . PHP_EOL;
        $print .= '    }' . PHP_EOL;
        return $print;
    }

    private function getID()
    {
        if (!$this->comTabela) {
            return false;
        }
        $print = PHP_EOL;
        $print .= '    /**' . PHP_EOL;
        $print .= '     * Retorna o valor de uma  chave primária' . PHP_EOL;
        $print .= '     *' . PHP_EOL;
        $print .= '     * @return mixed - valor da chave primaria' . PHP_EOL;
        $print .= '     */' . PHP_EOL;
        $print .= '    public function getID(){' . PHP_EOL;
        $chave = '';
        $chaves = $this->tabela->getChavePrimaria();
        if (sizeof($chaves) === 1) { 
            //var_dump($chaves); 
            $key = key($chaves);
            if($key === 'id'){
                $chave = '$this->id';
            }else{
                return '';
            }                  
        } elseif (sizeof($chaves) == 0) {
            echo '[Warning] - Tabela ' . ' possivelmente modelada errada - (sem chave primária)';
            $chave = 'false';
        } else {// chave composta
            $chave = '[ $this->' . rtrim(implode(', $this->', $chaves), ', $this->') . ']';
        }
        $print .= "        return " . $chave . ';' . PHP_EOL;
        $print .= '     }' . PHP_EOL;
        return $print;
    }

    private function getCondicao()
    {
        if (!$this->comTabela) {
            return false;
        }
        $print = PHP_EOL;
        $print .= '    /**' . PHP_EOL;
        $print .= '     * Utiliza como condição de seleção a chave primária' . PHP_EOL;
        $print .= '     *' . PHP_EOL;
        $print .= '     * @return string - Condição para selecionar um dado unico na tabela' . PHP_EOL;
        $print .= '     */' . PHP_EOL;
        $print .= '    public function getCondition()' . PHP_EOL . '    {' . PHP_EOL;
        $condicao = '';
        foreach ($this->tabela->getChavePrimaria() as $campo) {
            $condicao .= $campo->getNome() . " = ' . \$this->" . $campo->getVariavel() . ".' AND ";
        }
        $condicaoTrat = rtrim($condicao, ".' AND ");
        $print .= "        return '" . $condicaoTrat . ';' . PHP_EOL;

        $print .= '     }' . PHP_EOL;
        return $print;
    }

}
