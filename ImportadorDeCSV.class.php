<?php

define('VERSAO', '1.0.0 28/12/2023');

/**
 * Classe responsável por gerar classes.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class ImportadorDeCSV 
{



    public function __construct()
    {
        $this->trataForm();
    }


    private function trataForm()
    {
        $VERSAO = VERSAO;
        $TOOL = "Importador de CSV";
        $LIBS_EXTRA_JS = '';
        $SCRIPT = $this->getScripts();

        include __DIR__ . '/extras/cabecalho.phtml';

        if (isset($_POST['enviar'])) {
            $tabela = filter_input(INPUT_POST, 'tabela', FILTER_SANITIZE_STRING);
            $dados = $this->geraArquivoTemp();
            $RESULTADO = $this->verificar($tabela, $dados);
        } else if(isset($_POST['processar'])) {
            $RESULTADO =  $this->processarCSV($_POST['coluna'], $_POST['file'], $_POST['tabela']);
        } else { //Passo 1 sem contexto
            include __DIR__ . '/extras/csvImporter/csvArquivo.phtml';
            $RESULTADO = $nomeDao = $campos = '';
        }
        
        include __DIR__ . '/extras/resultado.phtml';

    }

    /**
     * Retorna um objeto do tipo arquivo para processar como CSV
     *
     * @return Arquivo
     */
    private function geraArquivoTemp():Arquivo
    {

        $folder = CACHE . '/files/';
        //Gera a pasta caso não exista
        if(!is_dir($folder)){
            mkdir($folder);
        }
        $arquivo = null;
        if(!empty($_POST['arquivoUrl'])){
            $arquivo = new ArquivoOnline($_POST['arquivoUrl']);
        }else{
            $arquivo = new ArquivoUpload('arquivo');
        }
       
        $arquivo->mover($folder);
        
        return $arquivo;
        
      
    }

    private function verificar($tabela, $arquivo, $separador = ',')
    {
        $colunas = call_user_func($tabela . '::getAtributes');
        array_unshift($colunas, '##IGNORAR##');

        $linhas = $colunasCSV  = [];
        $row = 0;
        if (($handle = fopen($arquivo->getArquivo(), "r")) !== FALSE) {
            $colunasCSV = $colunasTratadas = fgetcsv($handle, 10000, $separador);
            while (($data = fgetcsv($handle, 10000, $separador)) !== FALSE) {
                $linhas[] = $data;
                if ($row++ > 5) {
                    break;
                }
            }
            fclose($handle);
        }

        $colunasObj =  $colunas;
        $file= $arquivo->getNome();


        //Modelo padrão
        array_walk($colunasTratadas, function (&$v) {
            $v = lcfirst(StringUtil::spaceToCamelCase(StringUtil::removeAcentuacao($v)));
        });




        include __DIR__ . '/extras/csvImporter/csvProcesso.phtml';

    }

    private function processarCSV($mapa, $file, $tabela, $separador = ',')
    {
        $atributo = $mapa;
        dbd();
        $file = CACHE . 'files/' . $file;

        $row = 0;
        if (($handle = fopen($file, "r")) !== FALSE) {
            fgetcsv($handle, 10000, $separador); //descarta cabeçalho
            while (($data = fgetcsv($handle, 10000, $separador)) !== FALSE) {
                $linha = $data;
                $obj = new $tabela();

                foreach ($linha as $key => $value) {
                    if($atributo[$key] != '##IGNORAR##'){
                        $obj->{$atributo[$key]} = $value;                   
                    }
                }             

                ds($obj);
                $obj->save();


            }
            fclose($handle);
        }
    }

    private function getScripts()
    {
        #return file_get_contents(__DIR__ . '/geradorCrud/interface.js');
    }

    
    public function requires()
    {
        #require_once __DIR__ . '/geradorCrud/CampoGerador.class.php';

    }

   

}
