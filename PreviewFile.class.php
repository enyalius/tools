<?php

define('VERSAO', '1.0');

class PreviewFile
{

    private $files = array();

    public function __construct()
    {
        $this->requires();
        $VERSAO = VERSAO;
        $TOOL = "PreviewFile";

        $RESULTADO = $this->gerar();
        $LIBS_EXTRA_JS = '';

        
       
        include __DIR__ . '/extras/cabecalho.phtml';
        include __DIR__ . '/extras/resultado_free.phtml';
    }

    private function dependencias()
    {
        echo '<link rel="stylesheet" type="text/css" href="https://revisaonline.com.br/css/eny.css">';
        echo '<script src="https://revisaonline.com.br/js/dist/enyalius/main.js"></script>';

    }


    private function gerar()
    {
        ob_start();
        $this->dependencias();
        echo 'Verificando a existência do arquivo em z_data!' . PHP_EOL;


        $file = str_replace('..', '', $_GET['file']);//Evita acesso a arquivos fora da pasta destino
        echo '<pre>' . file_get_contents(ROOT .'/../z_data/classes/'.$file) .'</pre>';

        //saída
        $retorno = ob_get_contents();
        ob_end_clean();
        return $retorno;
    }

    private function requires()
    {
        // require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
    }

}
