<?php

define('VERSAO', '1.0 09/04/2023');

/**
 * Classe responsável por gerar classes.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class AddColuna{



    public function __construct()
    {
        $this->trataForm();
    }

   

    private function trataForm()
    {
        $VERSAO = VERSAO;
        $TOOL = 'Adicionar atributo/coluna';
        $LIBS_EXTRA_JS = '';

        if (isset($_POST['enviar'])) {
            $this->requires();
            $RESULTADO = $this->processa();
        } else {
            $RESULTADO = '';
        }
     
        
        include __DIR__ . '/extras/cabecalho.phtml';
        include __DIR__ . '/extras/addColuna/form.phtml';
        include __DIR__ . '/extras/resultado.phtml';

    }

    public function requires()
    {
        require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
    }

    private function processa(){
        extract($_POST);
        ob_start();
        print_r($_POST);
        if($this->localizaTabela($nomeTabela)){
            if(isset($tabelaBanco)){
                echo $this->criaAddColunm($nomeTabela, $nomeColuna, $colunaTipo);
            }
            if(isset($tabelaDocumentacao)){
                $this->addNaDocumentacao($nomeTabela, $nomeColuna, $colunaTipo);
            }
            if(isset($dto)){
                echo $this->dto();
            }
        }else{
            echo 'Não encontrei o objeto não posso continuar';
        }

        $retorno = ob_get_contents();
        ob_end_clean();
        return $retorno;
    }

    private function localizaTabela($tabela){
        return true;
    }

    private function criaAddColunm($tabela, $coluna, $tipo){
        $sql =  'ALTER TABLE ' . $tabela. ' ADD  COLUMN ' . $coluna .' ' . $tipo  .'; ';


        $model = new ModeloBD();
        try{
            $arquivo = ROOT . '../build/migrations/sql.sql';
            file_put_contents($arquivo, PHP_EOL . $sql, FILE_APPEND);

            $model->query($sql);
        }catch(Exception $e){
            echo PHP_EOL . 'OPS =>' . $e->getMessage() . PHP_EOL;
        }
        return $sql;
    }

    private function addNaDocumentacao($tabela, $coluna, $tipo){
        $arquivo = ROOT . '../doc/ModeloBD.vuerd.json';
        $estrutura =  json_decode(file_get_contents($arquivo));


        $objTmp = json_decode('{
            "id": "'. random_int(100, 999). 'acdea-ca06-'. random_int(10, 99) . 'a6-'. random_int(10, 99) . 'b8-8aa6f4a'. random_int(100, 999) . 'c1",
            "name": "' . $coluna. '",
            "comment": "",
            "dataType": "' .$tipo. '",
            "default": "",
            "option": {
              "autoIncrement": false,
              "primaryKey": true,
              "unique": false,
              "notNull": false
            },
            "ui": {
              "active": false,
              "pk": false,
              "fk": false,
              "pfk": false,
              "widthName": 60,
              "widthComment": 60,
              "widthDataType": 60,
              "widthDefault": 60
            }
          }');

        foreach($estrutura->table->tables as $obj){
            if($tabela == $obj->name){
                $obj->columns[] = $objTmp;
            }
        }
        file_put_contents($arquivo, json_encode($estrutura));

    }

    private function dto(){

    }
 

}
