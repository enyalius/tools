<?php

ini_set('max_execution_time', 300000);
ini_set('memory_limit', '4G');

define('VERSAO', '2.0');
define('DESCRICAO', 'Ferramenta que realiza o clone de um banco de dados de um servidor para outro.');

class CloneBD
{

    private $local = array();
    private $remoto = array();

    public function __construct()
    {
        $this->requires();
        $VERSAO = VERSAO;
        $DESCRICAO = DESCRICAO;
        $LIBS_EXTRA_JS = '';
        $TOOL = 'CloneBD';


        if (isset($_POST['enviar'])) {
            $this->lerVariaveis();
            $RESULTADO = $this->gerar();
        } else {
            $this->defaultValues();
            $RESULTADO = '';
        }
        $LOCAL = $this->local;
        $REMOTO = $this->remoto;

        include __DIR__ . '/extras/cabecalho.phtml';
        include __DIR__ . '/extras/CloneBD/form.phtml';
    }

    private function defaultValues()
    {
        $this->local['bd'] = $this->remoto['bd'] = DB_NAME;
        $this->local['server'] = DB_SERVER;
        $this->remoto['server'] = '';
        $this->local['usuario'] = $this->remoto['usuario']  = $this->remoto['usuario_ftp'] = DB_USER;
        $this->local['senha'] = $this->remoto['senha'] = DB_PASSWORD;
        $this->local['porta'] = 5432;
        $this->local['manterBancoAntigo'] = true;
        $this->local['ignorarLinhas'] = 0;
    }

    private function lerVariaveis()
    {
        $this->local['bd'] = filter_input(INPUT_POST, 'bdLocal', FILTER_SANITIZE_STRING);
        $this->local['usuario'] = filter_input(INPUT_POST, 'usuarioLocal', FILTER_SANITIZE_STRING);
        $this->local['senha'] = filter_input(INPUT_POST, 'senhaLocal', FILTER_SANITIZE_STRING);
        $this->local['server'] = filter_input(INPUT_POST, 'serverLocal', FILTER_SANITIZE_STRING);
        $this->local['porta'] = filter_input(INPUT_POST, 'porta', FILTER_SANITIZE_NUMBER_INT);
        $this->local['ignorarLinhas'] = filter_input(INPUT_POST, 'ignorarLinhas', FILTER_SANITIZE_NUMBER_INT);;


        $this->local['manterBancoAntigo'] = filter_input(INPUT_POST, 'manterBancoAntigo', FILTER_VALIDATE_BOOLEAN);

        $this->remoto['bd'] = filter_input(INPUT_POST, 'bdRemoto', FILTER_SANITIZE_STRING);
        $this->remoto['usuario'] = filter_input(INPUT_POST, 'usuarioRemoto', FILTER_SANITIZE_STRING);
        $this->remoto['senha'] = filter_input(INPUT_POST, 'senhaRemoto', FILTER_SANITIZE_STRING);
        $this->remoto['server'] = filter_input(INPUT_POST, 'serverRemoto', FILTER_SANITIZE_STRING);
        $this->remoto['senha_ftp'] = filter_input(INPUT_POST, 'senhaftp', FILTER_SANITIZE_STRING);
    }

    private function gerar()
    {
        

        ob_start();
        echo 'Iniciando o processo...' . PHP_EOL;
        

        $this->backupPreConditions();
        echo 'Começando o backup...' . PHP_EOL;

        $extra = isset($GLOBALS['CLONEBDEXTRA']) ? $GLOBALS['CLONEBDEXTRA'] : '';

        $file = $this->backupBDPG($extra);


        echo 'Instalando estrutura no servidor ' . PHP_EOL;
        $this->atualizaBD($file);

        $this->backupPosConditions();


        $tempo = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        echo "Processado em: " . $tempo . ' segundos';

        $retorno = ob_get_contents();
        ob_end_clean();
        return $retorno;
    }

    
    private function atualizaBD($backup)
    {
        require_once __DIR__ . '/extras/CloneBD/CloneModel.class.php';

        
        if ($this->local['manterBancoAntigo']) {
            $db = new ModeloBD($this->local['bd'], $this->local['server'], $this->local['usuario'], $this->local['senha']);
            $oldSchema = 'old_' . date('d_m_hi');
            $db->DB()->exec('ALTER SCHEMA public RENAME TO ' . $oldSchema);
            echo 'Schema atual renomeado para ' . $oldSchema . PHP_EOL;
        }

        echo 'Banco preparado para receber o Backup, iniciando...' . PHP_EOL;

        $model = new CloneModel($this->local);
        $model->restore($backup);

    }

    private function backupPreConditions()
    {
        echo 'Verificando passos adicionais' . PHP_EOL;

        $file = ROOT . '/../build/backup_pre_conditions.php';
        if (file_exists($file)) {
            echo 'Arquivo existe prosseguindo ...' . PHP_EOL;
            //Expondo variáveis
            $remoto = $this->remoto;
            $local = $this->local;
            require $file;
        } else {
            echo 'Arquivo NÃO existe. Seguindo com o normal' . PHP_EOL;
        }
    }

    private function backupPosConditions()
    {
        echo 'Verificando passos adicionais' . PHP_EOL;
        $remoto = $this->remoto;
        $local = $this->local;
        $file = ROOT . '/../build/backup_pos_conditions.php';
        if (file_exists($file)) {
            echo 'Arquivo existe prosseguindo ...' . PHP_EOL;
            require $file;
        } else {
            echo 'Arquivo NÃO existe.' . PHP_EOL;
        }
        echo 'FINALIZADO.' . PHP_EOL;
    }

    private function backupBDPG($extra = '')
    {
        $file = CACHE . '/dbbackup' . uniqid() . '.sql';
        #TODO ver de melhorar a questão de schemas diferentes
        $cmd = __DIR__ . '/extras/CloneBD/pg_dump ' . $extra . ' --dbname=postgresql://' . $this->remoto['usuario'] . ':' . $this->remoto['senha'] . '@' . $this->remoto['server'] . ':5432/' . $this->remoto['bd'] . "  --inserts --column-inserts  --no-privileges  --no-owner -n public | sed -e '/^--/d' > " . $file;
        echo $cmd . PHP_EOL . PHP_EOL;
        exec($cmd, $output);

        return $file;
    }


    private function requires()
    {
        require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
    }
}
