<?php

foreach($_POST['files'] as $file){

    $dest = str_replace('/tmp/eny/classes', ROOT, $file);
    
    $parts = pathinfo($dest);

    echo 'Copiando ' . $parts['basename'] . ' => ' .  $parts['dirname'] . PHP_EOL;

    if(!is_dir($parts['dirname'] )){
        mkdir($parts['dirname'], 0755, true );
    }
    if (!copy($file, $dest)) {
        echo "falha ao copiar $file...". PHP_EOL;
    }
}