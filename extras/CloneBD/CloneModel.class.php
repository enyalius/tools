<?php


class CloneModel{
    
    /**
     * Objeto com as configurações padrões para o ModeloBD.
     *
     * @var [object]
     */
    private $confs = null;
    
    
    public function __construct($confs)
    {
        $this->confs = new stdClass();
        foreach($confs as $key => $value){
            $this->confs->{$key} = $value;
        }
    }
    

    public function backupBDPG($extra = '')
    {
        $file = CACHE . '/dbbackup' . uniqid() . '.sql';
        
        $cmd = __DIR__ . '/extras/CloneBD/pg_dump ' . $extra . ' --dbname=postgresql://' . $this->confs->usuario . ':' . $this->confs['senha'] . '@' . $this->confs['server'] . ':5432/' . $this->confs['bd'] . "  --inserts --column-inserts  --no-privileges  --no-owner -T old* -n public | sed -e '/^--/d' > " . $file;
        echo $cmd . PHP_EOL . PHP_EOL;
        exec($cmd, $output);

        return $file;
    }

    public function renameSchema($schema){

    }

    public function removeSchema($schema){

    }
    
    public function restore($backup)
    {
        require_once __DIR__ . '/../../geradorCrud/ModeloBD.class.php';

        $db = new ModeloBD($this->confs->bd, $this->confs->server, $this->confs->usuario, $this->confs->senha);
        $db->DB()->debugOn();
        $db->DB()->exec('SET check_function_bodies = false;'); 

        echo '<pre>';
        $handle = file($backup);
        $exec = '';
        $count =  0;

        $ignoreLines = $this->confs->ignorarLinhas ?? 0;

        foreach ($handle as $linha) {
            
            //Ignora algumas linhas do arquivo útil para clones mais restritos
            if($ignoreLines > 0){
                $ignoreLines--;
                continue;
            }

            $count++;

            if ($count < 1000) {
                $exec .= $linha;
            } else {
               
                $insert =  substr($linha, 0, 6);

                if ($insert == 'INSERT') {
                    $count = 0;
                    try{
                        $db->DB()->exec($exec);
                    }catch(Exception $e){
                        ds($e->getMessage());
                        dd($e);
                    }
                    
                    $exec = $linha;
                } else {
                    $exec .= $linha;
                }
            }            
        }

        $db->DB()->exec($exec); //Executar o resto

    }
}