<?php

define('VERSAO', '1.0');

class GeradorBD
{

    private $files = array();

    public function __construct()
    {
        $this->requires();
        $VERSAO = VERSAO;
        $TOOL = "Gerador de banco de dados";
        
        if (isset($_POST['enviar'])) {
            $this->lerVariaveis();
            $RESULTADO = $this->gerar();
        } else {
            $RESULTADO = '';
        }
        include __DIR__ . '/extras/geradorBD/form.phtml';
    }

    private function lerVariaveis()
    {
      //  $this->local['bd'] = filter_input(INPUT_POST, 'bdLocal', FILTER_SANITIZE_STRING);
      
    }

    private function gerar()
    {
        ob_start();
        echo 'Enviando arquivos' . PHP_EOL;
        $views = $this->selecionaViews();
        echo 'Instalando no servidor ' . PHP_EOL;
        $this->atualizaViews($views);
        $retorno = ob_get_contents();
        ob_end_clean();
        return $retorno;
    }

   

    private function requires()
    {
        require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
    }

}
