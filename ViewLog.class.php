<?php

define('VERSAO', '1.1');

class ViewLog
{

    private $files = array();

    public function __construct()
    {
        $this->requires();
        $VERSAO = VERSAO;
        $TOOL = "VerLOGs";

        $RESULTADO = $this->gerar();
        
        $LIBS_EXTRA_JS = '<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>';
        $SCRIPT = file_get_contents(__DIR__.'/extras/viewLogs/script.js');
        
        include __DIR__ . '/extras/cabecalho.phtml';
        include __DIR__ . '/extras/resultado_free.phtml';
    }

    private function dependencias()
    {
        echo '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">';
    }
    
    
    private function processaMessage($message){
        $partes = explode('[[', $message);
        
        return [$partes[0], ''];
     }
    
    private function processaDetalhes($detalhes){
        $detalhes = str_replace("Detalhes]",'', $detalhes);
        $detalhes = substr($detalhes, 0, -2);
        $objeto = json_decode($detalhes);
        return $objeto;
    }

    private function gerar()
    {
        ob_start();
        $this->dependencias();
        echo 'Verificando a existência do arquivo de log em z_data!' . PHP_EOL;
        $file = ROOT . '/../z_data/ENY_ERROR_LOG.log';
        if (file_exists($file)) {
            echo 'Arquivo de log Existe prosseguindo ...' . PHP_EOL;
            echo '<table id="table" class="table table-striped table-bordered">';
            echo '<thead> <tr>';
            echo '   <th>Data</th>  <th>Tipo</th>   <th>User</th>   <th>Message</th>  <th> URL </th> <th> POST </th> <th> Navegador </th>';
            echo '</tr></thead>        <tbody>';
            $handle = @fopen($file, 'r');
            if ($handle) {
                while (($buffer = fgets($handle, 99905099)) !== false) {
                     echo '<tr>';
                    $erro = explode(']#[', $buffer);
                    echo '<td>' . $erro[0] .'</td>' ;
                    echo '<td>' . $erro[1] .'</td>' ;
                    echo '<td>' . $erro[2] .'</td>' ;
                    $message = $this->processaMessage($erro[3]);
                    echo '<td>' . htmlentities($message[0]) .'</td>' ;
                    $detalhes = $this->processaDetalhes( $erro[4] );
                    echo '<td>' . $detalhes->url .'</td>';
                    echo '<td>' . $detalhes->POST .'</td>';
                    echo '<td>' . $detalhes->userAgent .'</td>';
                    echo '</tr>';
                }
                if (!feof($handle)) {
                    echo "Error: unexpected fgets() fail\n";
                }
                fclose($handle);
                echo '</tbody> </table>';
            }
        } else {
            echo '<br><br>Arquivo de log NÃO EXISTE parabéns sua aplicação ainda não gerou nenhum erro.' . PHP_EOL;
        }
        $retorno = ob_get_contents();


        ob_end_clean();
        return $retorno;
    }

    private function requires()
    {
        // require_once __DIR__ . '/geradorCrud/ModeloBD.class.php';
    }

}
